#include<bits/stdc++.h>

using namespace std ;

int gc(int a, int b)
{
    if(b == 0)
        return 1;
    else if(a < b)
        return gc(b, a);
    else
    {
        return 1+gc(b, a%b);
    }
    
}

int main()
{
    int t;
    long long n, m;
    cin >> t ;
    while(t--)
    {
        cin >> n >> m ;
        if(gc(n, m) % 2 == 0)
            cout << "Ari\n" ;
        else
        {
            cout << "Rich\n" ;
        }
    }
    return 0;
}