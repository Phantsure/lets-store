#include<bits/stdc++.h>

using namespace std;

int main()
{
    int t, n, tmp;
    cin >> t ;
    while(t--)
    {
        int pos=0, neg=0;
        cin >> n ;
        while(n--)
        {
            cin >> tmp ;
            if(tmp > 0)
                pos++;
            else
            {
                neg++;
            }
        }
        if(pos != 0 && neg != 0)
            if(pos > neg)
                cout << pos << ' ' << neg << endl ;
            else
            {
                cout << neg << ' ' << pos << endl ;
            }
        else
        {
            cout << pos+neg << ' ' << pos+neg << endl ;
        }
    }
    return 0 ;
}